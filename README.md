# SC4S/Kernel UDP Input Buffer Error

SC4S has a Kernel setting that requests a certain buffer size when configuring the UDP sockets. The kernel must have its parameters set to at least the same size _(or greater)_ than the syslog-ng config is requesting or the following will occur in the SC4S logs.

```bash
[2022-02-16T08:45:35.912591] The kernel refused to set the receive buffer (SO_RCVBUF) to the requested size, you \ 
probably need to adjust buffer related kernel parameters; so_rcvbuf='17039360', so_rcvbuf_set='3407872'
```

The current `/etc/systctl.conf` has values of.
```bash
net.core.rmem_default=1703936
net.core.rmem_max=1703936
```

To mitigate this we will deploy new values to `/etc/sysctl.conf` apply to the running kernel, so that the buffer size is not hit and restart the sc4s service via [podman/systemd](splunk/splunk-connect-for-syslog/-/blob/master/roles/sc4s/tasks/main.yml). 

#### Components
- [tasks/sysctl_udp.yml](tasks/sysctl_udp.yml) - Backup `/etc/sysctl.conf` && change `net.core.rmem_default net.core.rmem_max` as per `vars/main.yml`
- [tasks/sc4s_restart.yml](tasks/sc4s_restart.yml) - Restart SC4S
- [tasks/changelog_update.yml](tasks/changelog_update.yml) - Update `/var/log/CHANGELOG`

#### Installation

As we only want this role to be deployed against the SC4S/skg hosts, we will use the `[splunk-servers]` inventory host grouping and no others.

```bash
$ ansible-playbook deploy.yml -b -K
```

To check the validity of the changes made and if they were successful.
```bash
$ ansible splunk-servers -m shell -a "grep 5111808 /etc/sysctl.conf ; systemctl status sc4s| grep running"
```

#### Issues
~~Current versions of SC4S 1.6 have encountered a [bug](https://github.com/splunk/splunk-connect-for-syslog/issues/1406) where the deployed values are ignored, so until this issue is fixed the issue remains.~~

